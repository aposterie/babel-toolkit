import t from '@babel/types'

declare module '@babel/types' {
  export function inherits<T>(child: T, parent: any): T;
  export function ensureBlock(node: Node | Node[], key?: string): t.Block
  export function isReferenced(node: Node, parent: Node): boolean
  export function isValidIdentifier(id: string): boolean

  interface TSEnumDeclaration {
    const: boolean;
  }
}