import t from '@babel/types'
import * as traverse from '@babel/traverse'
import { Visitor, NodePath } from '@babel/traverse'

declare global {
  type PluginObject<S = any> = {
    pre?(): void
    visitor?: Visitor<S>;
  }

  type Plugin<S = any> = (babel: {
    version: typeof import('@babel/core').version;
    types: typeof t
    template: typeof import('@babel/template')
  }) => PluginObject<S>

  type types = typeof t;
  
  interface Array<T> {
    reduce<U>(callback: (accum: U, current: T, index: number, array: T[]) => U): U;
  }  

  type NodePath<T> = traverse.NodePath<T>
  type Node = traverse.Node;
  type Scope = /* traverse.Scope & */ {
    [key: string]: any
    generateUidIdentifier(name: string): Identifier
    generateUidIdentifierBasedOnNode(id: string)
    hasBinding(name: string): boolean
    hasOwnBinding(name: string): boolean
    rename(name: string, to?: string): void
    parent: NodePath<Node>
  };
  
  // type GenericTypeAnnotation = t.GenericTypeAnnotation;
  type ArrowFunctionExpression = t.ArrowFunctionExpression;
  type AssignmentPattern = t.AssignmentPattern;
  type BinaryExpression = t.BinaryExpression;
  type Block = t.Block;
  type BlockStatement = t.BlockStatement;
  type Class = t.Class;
  type CallExpression = t.CallExpression;
  type Declaration = t.Declaration;
  type ExportNamedDeclaration = t.ExportNamedDeclaration;
  type Expression = t.Expression;
  type ExpressionStatement = t.ExpressionStatement;
  type ForOfStatement = t.ForOfStatement;
  type ForStatement<Body extends Statement = Statement> = t.ForStatement & { body: Body };
  type FunctionDeclaration = t.FunctionDeclaration;
  type FunctionExpression = t.FunctionExpression;
  type Identifier = t.Identifier;
  type IfStatement = t.IfStatement;
  type ImportDeclaration = t.ImportDeclaration;
  type Literal = t.Literal;
  type LogicalExpression = t.LogicalExpression;
  type MemberExpression = t.MemberExpression;
  type NumericLiteral = t.NumericLiteral;
  type ObjectExpression = t.ObjectExpression;
  type ObjectMethod = t.ObjectMethod;
  type ObjectPattern = t.ObjectPattern;
  type ObjectProperty = t.ObjectProperty;
  type Program = t.Program;
  type ReturnStatement = t.ReturnStatement;
  type SpreadElement = t.SpreadElement;
  type Statement = t.Statement;
  type StringLiteral = t.StringLiteral;
  type SwitchCase = t.SwitchCase;
  type TSInterfaceDeclaration = t.TSInterfaceDeclaration;
  type TSLiteralType = t.TSLiteralType;
  type TSParameterProperty = t.TSParameterProperty;
  type TSQualifiedName = t.TSQualifiedName;
  type TSType = t.TSType;
  type TSTypeAliasDeclaration = t.TSTypeAliasDeclaration;
  type TSTypeAnnotation = t.TSTypeAnnotation;
  type TSTypeLiteral = t.TSTypeLiteral;
  type TypeAlias = t.TypeAlias;
  type UnaryExpression = t.UnaryExpression;
  type VariableDeclaration = t.VariableDeclaration;
}