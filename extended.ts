import 'babel-types'

declare module 'babel-types' {
  export function isValidIdentifier(identifier: string): boolean
  export function isImport(importee: Node): boolean
}