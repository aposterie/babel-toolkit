

/**
 * This plugin replaces the elaborate TypeScript enum output with a
 * beautiful object literal. It should also works with minified
 * codes.
 *
 * e.g.
 *
 *    (function (Direction) {
 *      Direction[Direction["Up"] = 1] = "Up";
 *      Direction[Direction["Down"] = 2] = "Down";
 *      Direction[Direction["Left"] = 3] = "Left";
 *      Direction[Direction["Right"] = 4] = "Right";
 *    })(Direction || (Direction = {}));
 *
 * transpiles to
 *
 *    var Direction = {
 *      1: "Up",
 *      Up: 1,
 *      2: "Down",
 *      Down: 2,
 *      3: "Left",
 *      Left: 3,
 *      4: "Right",
 *      Right: 4
 *    };
 */
export default ({ constEnum = false, excludes = [] as string[] }): Plugin => ({ types: t }) => {
  return {
    visitor: {
      TSEnumDeclaration({ node }) {
        node.const = false;
      },
      ExpressionStatement(path: NodePath<ExpressionStatement>) {
        const name = isEnumIIFE(path.node)
        if (!name || excludes.includes(name)) return

        const enumeration: ObjectProperty[] = []
        path.traverse({
          AssignmentExpression({ node }) {
            const left = node.left as MemberExpression
            const property = left.property
            if (!property) return
            let key = t.isAssignmentExpression(property)
              ? property.right
              : property

            // Remove the quote if possible
            if (t.isStringLiteral(key) && t.isValidIdentifier(key.value)) {
              key = t.identifier(key.value)
            }

            // Removes reverse lookup (int to string)
            if (constEnum && t.isNumericLiteral(key)) return

            enumeration.push(t.objectProperty(key, node.right))
          },
        })

        // Remove preceding `var`
        // const previousSibling = path.getSibling((<any>path.key) - 1 as any).node
        // if (
        //   Array.isArray(path.container)
        //   && t.isVariableDeclaration(previousSibling)
        //   && previousSibling.declarations.length === 1
        // ) {
        //   const { id, init } = previousSibling.declarations[0]
        //   if (t.isIdentifier(id, { name }) && init === null) {
        //     path.container.splice((<any>path.key) - 1, 1)
        //   }
        // }

        const objectAssign = t.memberExpression(t.identifier('Object'), t.identifier('assign'))

        path.replaceWith(
          t.callExpression(objectAssign, [
            path.node.expression.arguments[0],
            t.objectExpression(enumeration)
          ])
        );
      }
    },
  };

  /** Matches `B || (B = {})` or `B = B || (B = {})` */
  function isEnumFunctionArgument({ operator, left, right }: LogicalExpression) {
    return (
      operator === '||' &&
      t.isIdentifier(left) &&
      t.isAssignmentExpression(right) &&
      right.operator === '=' &&
      t.isIdentifier(right.left, { name: left.name }) &&
      t.isObjectExpression(right.right) &&
      !right.right.properties.length
    )
  }

  function isEnumIIFE({ expression }: ExpressionStatement): string | undefined {
    // Checks function wrapper
    if (!t.isCallExpression(expression)) return
    const { callee, arguments: args } = expression
    if (!t.isFunctionExpression(callee)) return
    const { params, body } = callee
    if (!t.isBlockStatement(body)) return
    if (params.length !== 1) return
    const param = params[0]
    if (!t.isIdentifier(param)) return
    // @FIXME this does not have to equal to the argument name
    const { name } = param

    // Checks arguments
    if (args.length !== 1) return

    // Matches (A || (A = {}))
    const arg = args[0]
    if (t.isLogicalExpression(arg)) {
      if (!isEnumFunctionArgument(arg)) return
    } else if (t.isAssignmentExpression(arg)) {
      const { left, right } = arg
      if (!(
        t.isIdentifier(left) &&
        t.isLogicalExpression(right) &&
        isEnumFunctionArgument(right)
      )) return
    }

    // Checks every statement in the function block is in TypeScript enum format.
    let nodes: (Expression | void)[] = body.body.map(_ => (_ as any).expression)
    // For minified codes `(a.b = b, a.c = c, a.d = d)` that are collapsed into one statement
    if (body.body.length === 1) {
      const node = body.body[0]
      if (t.isExpressionStatement(node) && t.isSequenceExpression(node.expression)) {
        nodes = node.expression.expressions
      }
    }

    if (!nodes.every(node => {
      if (!node || !t.isAssignmentExpression(node)) return false
      const { left, right } = node
      if (!t.isMemberExpression(left)) return false
      if (!t.isStringLiteral(right)) return false
      const { object, property } = left
      if (!t.isIdentifier(object, { name })) return false

      if (t.isAssignmentExpression(property)) {
        const { left, right } = property
        return t.isMemberExpression(left)
          && t.isIdentifier(left.object, { name })
          && t.isStringLiteral(right) || t.isNumericLiteral(right)
      } else {
        return t.isStringLiteral(property) || t.isIdentifier(property)
      }
    })) return

    return name
  }
}