

/**
 * Remove Symbol() description for production
 * @example
 * 
 * babel.transform(code, {
 *   plugins: [
 *     require('babel-plugin-strip-symbol-description'),
 *   ],
 * })
 */
export default (): Plugin => ({ types: t }) => ({
  visitor: {
    CallExpression(path: NodePath<CallExpression>) {
      const { node } = path;
      if (
        t.isIdentifier(node.callee, { name: 'Symbol' }) &&
        node.arguments.length === 1
      ) {
        node.arguments = []
      }
    }
  }
})
