import { map } from "lodash"
import { none, generateUidIdentifier } from '../@util';
import * as runtime from "./runtime"

export * from "./runtime"

const packageName = require('./package.json').name;

export default (): Plugin => ({ types: t }) => {
  const ids: { [key in keyof typeof runtime]: Identifier } = {} as any

  function functionCall(variety: Identifier, ...expressions: Expression[]) {
    return t.callExpression(variety, expressions)
  }

  function buildTypeLiteral(type: TSTypeLiteral, scope: Scope) {
    return t.callExpression(ids.object, [t.objectExpression(type.members.flatMap((member) => {
      if (t.isTSPropertySignature(member)) {
        return t.objectProperty(member.key, reify(member.typeAnnotation!.typeAnnotation, scope))
      }
      return [] as ObjectProperty[]
    }))])
  }
  
  function reify(type: TSType, scope: Scope): Expression {
    switch (type.type) {
      case 'TSNumberKeyword':
        return t.stringLiteral('number')
      case 'TSStringKeyword':
        return t.stringLiteral('string')
      case 'TSBooleanKeyword':
        return t.stringLiteral('boolean')
      case 'TSUndefinedKeyword':
        return t.identifier('undefined')
      case 'TSObjectKeyword':
        return t.stringLiteral('object')
      case 'TSNullKeyword':
        return t.nullLiteral()
      case 'TSUnknownKeyword' as any:
        return t.stringLiteral('unknown')
      case 'TSBigIntKeyword' as any:  
        return t.stringLiteral('bigint')
      case 'TSLiteralType':
        return t.isStringLiteral(type.literal)
          ? functionCall(ids.string, type.literal)
          : type.literal
      case 'TSUnionType':
        return functionCall(ids.union, ...type.types.map(t => reify(t, scope)))
      case 'TSArrayType':
        return functionCall(ids.array, reify(type.elementType, scope))
      case 'TSTypeReference':
        if (t.isIdentifier(type.typeName)) {
          if (scope.hasBinding(type.typeName.name)) {
            return functionCall(ids.ref, type.typeName)
          }
        } else {
          type.typeName as TSQualifiedName
        }
        break

      case 'TSTypeLiteral':
        return buildTypeLiteral(type, scope)
    }
    return t.stringLiteral('any')
  }

  const visitor = {
    TSTypeAliasDeclaration(path: NodePath<TSTypeAliasDeclaration>) {
      const { node, scope } = path
      path.replaceWith(t.variableDeclaration("var", [
        t.variableDeclarator(
          node.id,
          t.callExpression(ids.typeAlias, [reify(node.typeAnnotation, scope)])
        )
      ]))
    },

    TSInterfaceDeclaration(path: NodePath<TSInterfaceDeclaration>) {

    },

    FunctionDeclaration({ node, scope }: NodePath<FunctionDeclaration>) {
      const { body, params } = node;
      const addedNodes: Node[] = [];
      
      for (const param of params) {
        const { typeAnnotation } = param as Identifier
        if (!t.isTSTypeAnnotation(typeAnnotation!)) continue;
        if (t.isIdentifier(param)) {
          const typeOf = reify(typeAnnotation.typeAnnotation, scope)
          if (typeOf) {
            addedNodes.push(t.expressionStatement(
              t.callExpression(ids.expect, [
                /* paramName */ t.stringLiteral(param.name),
                /* actualValue */ param,
                /* expected */ typeOf,
              ])
            ))
          }
        }
      }
      body.body.unshift(...addedNodes);
    }
  }

  return {
    visitor: {
      Program(path: NodePath<Program>) {
        const { node, scope } = path
        if (none(node.body, statement =>
          t.isImportDeclaration(statement) &&
          t.isStringLiteral(statement.source, { value: packageName })
        )) {
          const specifiers = map(runtime, (fn, key) => {
            ids[key] = generateUidIdentifier(scope, t.identifier(fn.name))
            return t.importSpecifier(ids[key], t.identifier(fn.name))
          })
          node.body.unshift(t.importDeclaration(specifiers, t.stringLiteral(packageName + "/runtime")))
        }
        path.traverse(visitor)
      },
    }
  }
}