enum Type {
  Union = "@union",
  Array = "@array",
  Ref = "@ref",
  Obj = "@obj",
  Str = "@str",
  Intersection = "@intersect",
}

export function union(...v) {
  return [Type.Union, ...v];
}
export function array(v) {
  return [Type.Array, v];
}
export function string(v) {
  return [Type.Str, v];
}
export function ref(v) {
  return [Type.Ref, v];
}
export function object(value: { [key: string]: TypeInfo }) {
  return [Type.Obj, value]
}

type TypeInfo = string | number | bigint | boolean | null | undefined | [Type, any]

function checkType(actual: any, expected: TypeInfo) {
  switch (expected) {
    case 'number':
    case 'string':
    case 'function':
    case 'boolean':
    case 'object':
      return typeof actual !== expected
    case 'any':
    case 'unknown':
      return true
  }

  if (Array.isArray(expected)) {
    const [variety, values] = expected
    switch (variety) {
      case Type.Union:
        return values.some(_ => checkType(actual, _))
      case Type.Array:
        return checkType(actual, values)
      case Type.Ref:
        return actual instanceof values
      case Type.Str:
        return actual === expected
      case Type.Obj:
        return expected != null && Object.keys(expected).every(key => checkType(actual[key], expected[key]))
    }
    return true
  }
  if (typeof expected === "string" && expected[0] === '"') {
    return actual === JSON.parse(expected)
  } 
  if (typeof expected === "number" || typeof expected === "bigint" || typeof expected === "boolean" || expected == null) {
    return actual === expected
  }
}

export function expect(paramName: string, actual: any, expected: any) {
  if (!checkType(actual, expected)) {
    throw TypeError(`${paramName} should be "${expected}" but got ${actual} instead.`)
  }
}

export function typeAlias(value: TypeInfo) {
  return value;
}

