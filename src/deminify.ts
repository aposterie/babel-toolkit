import { defineProperty } from './deminify/object-defineproperty';
import { defaultParam } from './deminify/default-param';
import { depolyfill } from './deminify/depolyfill';
import { rename } from './deminify/rename';
import { deblock } from './deminify/deblock';
import { desequence } from './deminify/desequence';
import { logicalExpressionToIf } from './deminify/logical-expression-to-if';
import { removeDeadCode } from './deminify/deadcode';
import { iifeTypeScriptClass } from './deminify/iife-typescript-class';

export default {
  plugins: [
    defaultParam,
    depolyfill,
    desequence,
    logicalExpressionToIf,
    defineProperty,
    rename,
    deblock,
    removeDeadCode,
    iifeTypeScriptClass,
  ]
}