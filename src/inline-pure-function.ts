/**
 * Inline `fn()` to its contents
 */
export default (...names: string[]): Plugin => ({ types: t }) => ({
  visitor: {
    CallExpression(path: NodePath<CallExpression>) {
      for (const name of names)
      if (t.isIdentifier(path.node.callee, { name })) {
        let p: NodePath<Node> = path
        while (!p.scope.hasOwnBinding(name)) {
          p = p.parentPath
        }
        const node = p.scope.bindings[name].path.node
        if (
          !t.isVariableDeclarator(node) ||
          !t.isArrowFunctionExpression(node.init!) ||
          node.init.params.length
        ) return
        path.replaceWith(node.init.body)
      }
    }
  }
})
