
import { orElse } from './@util'
import createEmotion, { Options as EmotionOptions } from 'create-emotion'

// Cheap `Symbol` alternative
// https://en.wikipedia.org/wiki/Sentinel_value
const sentinel = {}

interface Options extends EmotionOptions {
  onCSS(text: string): void
}
interface State extends Node {
  hasEmotionCSSImport: boolean | null
}

/**
 * Remove `emotion.css` calls to move them to a static file
 * @example
 * 
 * babel.transform(code, {
 *   plugins: [
 *     require("babel-plugin-emotion-strip")({
 *       // create-emotion options
 *       prefix: false,
 *       // callback for new CSS texts 
 *       onCSS(value: string) {
 *         fs.appendFileSync("dist/style.css", value)
 *       },
 *     })
 *   ]
 * })
 */
export default ({ onCSS, ...opts }: Options): Plugin => ({ types: t }) => {
  const emotion = createEmotion(opts)
  const classNamePrefix = opts.key || 'css'

  // Inline constant numeric or string arguments to `css` calls
  function isLiteralWithConstantValue(departure: NodePath<Node>, arg: Identifier) {
    for (let path = departure; path; path = path.parentPath) {
      const body: Statement[] = path.node['body']
      if (!Array.isArray(body)) continue
      for (const statement of body) {
        if (!t.isVariableDeclaration(statement, { kind: 'const' })) continue
        for (const dec of statement.declarations) {
          if (!t.isIdentifier(dec.id, { name: arg.name })) continue
          if (!t.isNumericLiteral(dec.init!) && !t.isStringLiteral(dec.init!)) continue
          return dec.init.value
        }
      }
    }
  }

  return {
    pre(this: State) {
      this.hasEmotionCSSImport = null;
    },
    visitor: {
      CallExpression(this: State, path: NodePath<CallExpression>) {
        const { node } = path
        if (this.hasEmotionCSSImport == null) {
          const program = path.findParent(t.isProgram).node as Program
          this.hasEmotionCSSImport = program.body.some(statement =>
            t.isImportDeclaration(statement) &&
            statement.source.value === 'emotion' &&
            statement.specifiers.some(specifier =>
              t.isImportSpecifier(specifier) &&
              t.isIdentifier(specifier.imported, { name: 'css' })
            )
          )
        }
        if (!this.hasEmotionCSSImport) return

        const args = node.arguments.map(arg => {
          if (t.isStringLiteral(arg) || t.isNumericLiteral(arg)) return arg.value
          if (t.isIdentifier(arg)) return orElse(isLiteralWithConstantValue(path, arg), sentinel)
          return sentinel
        })

        if (
          t.isIdentifier(node.callee, { name: 'css' }) &&
          args.length &&
          args.every(arg => arg !== sentinel)
        ) {
          const values = args as (string|number)[]
          const className = emotion.css(...values)
          onCSS(emotion.cache.inserted[className.replace(classNamePrefix + '-', '')] as string)
          path.replaceWith(t.stringLiteral(className))
        }
      }
    }
  }
}
