// f48b47c
import syntaxTypeScript from "@babel/plugin-syntax-typescript";
import { injectInitialization } from "@babel/helper-create-class-features-plugin";

import transpileEnum from "@babel/plugin-transform-typescript/lib/enum";
import transpileNamespace from "@babel/plugin-transform-typescript/lib/namespace";

import { GLOBAL_TYPES, isGlobalType } from "./util"
import { dropTypeImports } from './drop-type-imports';

const PARSED_PARAMS = new WeakSet();

interface Options {
  jsxPragma: string
  allowNamespaces: boolean
  keepTypeImports: boolean
}
interface State {
  file: any
}

const JSX_ANNOTATION_REGEX = /\*?\s*@jsx\s+([^\s]+)/;

export default
  ({ jsxPragma = "React", allowNamespaces, keepTypeImports }: Partial<Options> = {}): Plugin<State> =>
  ({ types: t, template }) => {

  function visitPattern({ node }) {
    if (node.typeAnnotation) node.typeAnnotation = null;
    if (t.isIdentifier(node) && node.optional) node.optional = null;
    // 'access' and 'readonly' are only for parameter properties, so constructor visitor will handle them.
  }

  return {
    name: "transform-typescript",
    inherits: syntaxTypeScript,

    visitor: {
      //"Pattern" alias doesn't include Identifier or RestElement.
      Pattern: visitPattern,
      Identifier: visitPattern,
      RestElement: visitPattern,

      Program(path, state) {
        const { file } = state;
        let fileJsxPragma = jsxPragma;

        if (!GLOBAL_TYPES.has(path.node)) {
          GLOBAL_TYPES.set(path.node, new Set());
        }

        if (file.ast.comments) {
          for (const comment of file.ast.comments) {
            const jsxMatches = JSX_ANNOTATION_REGEX.exec(comment.value);
            if (jsxMatches) {
              fileJsxPragma = jsxMatches[1];
            }
          }
        }

        // remove type imports
        if (!keepTypeImports) {
          dropTypeImports(path, fileJsxPragma)
        }
      },

      ExportNamedDeclaration(path) {
        // remove export declaration if it's exporting only types
        if (
          !path.node.source &&
          path.node.specifiers.length > 0 &&
          path.node.specifiers.every((node) =>
            "local" in node && isGlobalType(path, node.local.name),
          )
        ) {
          path.remove();
        }
      },

      ExportSpecifier(path) {
        // remove type exports
        if (!(path.parent as ExportNamedDeclaration).source && isGlobalType(path, path.node.local.name)) {
          path.remove();
        }
      },

      ExportDefaultDeclaration(path) {
        // remove whole declaration if it's exporting a TS type
        if (
          t.isIdentifier(path.node.declaration) &&
          isGlobalType(path, path.node.declaration.name)
        ) {
          path.remove();
        }
      },

      TSDeclareFunction(path) {
        path.remove();
      },

      TSDeclareMethod(path) {
        path.remove();
      },

      VariableDeclaration(path) {
        if (path.node.declare) {
          path.remove();
        }
      },

      VariableDeclarator({ node }) {
        if (node.definite) node.definite = null;
      },

      ClassMethod(path) { 
        const { node } = path;

        if (node.accessibility) node.accessibility = null;
        if (node.abstract) node.abstract = null;
        if (node.optional) node.optional = null;

        // Rest handled by Function visitor
      },

      ClassProperty(path) {
        const { node } = path;

        if (node.accessibility) node.accessibility = null;
        if (node.abstract) node.abstract = null;
        if (node.readonly) node.readonly = null;
        if (node.optional) node.optional = null;
        if (node.definite) node.definite = null;
        if (node.typeAnnotation) node.typeAnnotation = null;
      },

      TSIndexSignature(path) {
        path.remove();
      },

      ClassDeclaration(path) {
        const { node } = path;
        if (node.declare) {
          path.remove();
          return;
        }
      },

      Class(path) {
        const { node } = path

        if (node.typeParameters) node.typeParameters = null;
        if (node.superTypeParameters) node.superTypeParameters = null;
        if (node.implements) node.implements = null;
        if (node.abstract) node.abstract = null;

        // Similar to the logic in `transform-flow-strip-types`, we need to
        // handle `TSParameterProperty` and `ClassProperty` here because the
        // class transform would transform the class, causing more specific
        // visitors to not run.
        path.get("body.body").forEach(child => {
          const childNode = child.node;

          if (t.isClassMethod(childNode, { kind: "constructor" })) {
            // Collects parameter properties so that we can add an assignment
            // for each of them in the constructor body
            //
            // We use a WeakSet to ensure an assignment for a parameter
            // property is only added once. This is necessary for cases like
            // using `transform-classes`, which causes this visitor to run
            // twice.
            const parameterProperties: (AssignmentPattern | Identifier)[] = [];
            for (const param of childNode.params) {
              if (
                t.isTSParameterProperty(param) &&
                !PARSED_PARAMS.has(param.parameter)
              ) {
                PARSED_PARAMS.add(param.parameter);
                parameterProperties.push(param.parameter);
              }
            }

            if (parameterProperties.length) {
              const assigns = parameterProperties.map(p => {
                let id;
                if (t.isIdentifier(p)) {
                  id = p;
                } else if (
                  t.isAssignmentPattern(p) &&
                  t.isIdentifier(p.left)
                ) {
                  id = p.left;
                } else {
                  throw path.buildCodeFrameError(
                    "Parameter properties can not be destructuring patterns.",
                  );
                }

                return template.statement.ast`this.${id} = ${id}`;
              });

              injectInitialization(path, child, assigns);
            }
          } else if (child.isClassProperty()) {
            childNode.typeAnnotation = null;

            if (!childNode.value && !childNode.decorators) {
              child.remove();
            }
          }
        });
      },

      Function({ node }) {
        if (node.typeParameters) node.typeParameters = null;
        if (node.returnType) node.returnType = null;

        const p0 = node.params[0];
        if (p0 && t.isIdentifier(p0) && p0.name === "this") {
          node.params.shift();
        }

        // We replace `TSParameterProperty` here so that transforms that
        // rely on a `Function` visitor to deal with arguments, like
        // `transform-parameters`, work properly.
        node.params = node.params.map(p => {
          return p.type === "TSParameterProperty" ? p.parameter : p;
        });
      },

      TSModuleDeclaration(path) {
        transpileNamespace(path, t, allowNamespaces);
      },

      TSInterfaceDeclaration(path) {
        path.remove();
      },

      TSTypeAliasDeclaration(path) {
        path.remove();
      },

      TSEnumDeclaration(path) {
        transpileEnum(path, t);
      },

      TSImportEqualsDeclaration(path) {
        const { node } = path
        const ref = node.moduleReference

        const qualifiedNameToMemberExpression = (node: TSQualifiedName) =>
          t.memberExpression(
            t.isTSQualifiedName(node.left)
              ? qualifiedNameToMemberExpression(node.left)
              : node.left,
            node.right
          )

        const right =
          t.isIdentifier(ref) ? ref :
          t.isTSQualifiedName(ref) ? qualifiedNameToMemberExpression(ref)
          : ref.expression

        path.replaceWith(
          t.variableDeclaration("var", [
            t.variableDeclarator(
              path.node.id,
              t.callExpression(
                t.identifier("require"),
                [right]
              )
            )
          ])
        )
      },

      TSExportAssignment(path) {
        throw path.buildCodeFrameError(
          "`export =` is not supported by @babel/plugin-transform-typescript\n" +
            "Please consider using `export <value>;`.",
        );
      },

      TSTypeAssertion(path) {
        path.replaceWith(path.node.expression);
      },

      TSAsExpression(path) {
        let node: Expression = path.node;
        do {
          node = node.expression;
        } while (t.isTSAsExpression(node));
        path.replaceWith(node);
      },

      TSNonNullExpression(path) {
        path.replaceWith(path.node.expression);
      },

      CallExpression(path) {
        path.node.typeParameters = null;
      },

      NewExpression(path) {
        path.node.typeParameters = null;
      },

      JSXOpeningElement(path) {
        path.node.typeParameters = null;
      },

      TaggedTemplateExpression(path) {
        path.node.typeParameters = null;
      },
    },
  };
}