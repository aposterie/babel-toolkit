//
// https://github.com/Microsoft/TypeScript/wiki/FAQ#why-are-imports-being-elided-in-my-emit
// https://github.com/microsoft/TypeScript/issues/9191
// https://github.com/babel/babel/issues/9723
// https://github.com/babel/babel/issues/8358

/**
 * TypeScript removes unused imports automatically, unfortunately
 * we cannot predict if they are used in a future cell. This forces
 * Babel to preserve the imports.
 */
export default (): Plugin => () => ({
  visitor: {
    Program(topPath) {
      // Run from top level otherwise TypeScript plugin is faster than us.
      topPath.traverse({
        ImportDeclaration(path) {
          const { node } = path
          if (!node.specifiers.length) return

          // path.scope.hasBinding doesn't seem to reflect new top-level declarations
          // https://github.com/babel/babel/issues/8358
          node.specifiers
            .map(s => s.local.name)
            .map(n => path.scope.getBinding(n)!.referencePaths)
            .forEach(paths => {
              paths.push({ parent: { type: "Identifier" } } as any)
            })
        }
      })
    }
  },
})
