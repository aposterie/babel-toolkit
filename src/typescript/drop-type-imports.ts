import * as t from "@babel/types"
import { registerGlobalType, isInType } from './util';

export function dropTypeImports(path: NodePath<Program>, fileJsxPragma?: string) {
  for (let stmt of path.get("body")) {
    if (t.isImportDeclaration(stmt.node)) {
      // Note: this will allow both `import { } from "m"` and `import "m";`.
      // In TypeScript, the former would be elided.
      if (stmt.node.specifiers.length === 0) {
        continue;
      }

      let allElided = true;
      const importsToRemove: NodePath<Node>[] = [];

      for (const specifier of stmt.node.specifiers) {
        const binding = stmt.scope.getBinding(specifier.local.name);

        // The binding may not exist if the import node was explicitly
        // injected by another plugin. Currently core does not do a good job
        // of keeping scope bindings synchronized with the AST. For now we
        // just bail if there is no binding, since chances are good that if
        // the import statement was injected then it wasn't a typescript type
        // import anyway.
        if (
          binding &&
          isImportTypeOnly({
            binding,
            programPath: path,
            jsxPragma: fileJsxPragma,
          })
        ) {
          importsToRemove.push(binding.path);
        } else {
          allElided = false;
        }
      }

      if (allElided) {
        stmt.remove();
      } else {
        for (const importPath of importsToRemove) {
          importPath.remove();
        }
      }

      continue;
    }

    if (stmt.isExportDeclaration()) {
      stmt = stmt.get("declaration") as NodePath<Declaration>;
    }

    if (stmt.isVariableDeclaration({ declare: true })) {
      for (const name of Object.keys(stmt.getBindingIdentifiers())) {
        registerGlobalType(path.scope, name);
      }
    } else if (
      stmt.isTSTypeAliasDeclaration() ||
      stmt.isTSDeclareFunction() ||
      stmt.isTSInterfaceDeclaration() ||
      stmt.isClassDeclaration({ declare: true }) ||
      stmt.isTSEnumDeclaration({ declare: true }) ||
      (stmt.isTSModuleDeclaration({ declare: true }) &&
        stmt.get("id").isIdentifier())
    ) {
      registerGlobalType(path.scope, (stmt.node!.id as Identifier).name);
    }
  }
}

function isImportTypeOnly({ binding, programPath, jsxPragma }) {
  for (const path of binding.referencePaths) {
    if (!isInType(path)) {
      return false;
    }
  }

  if (binding.identifier.name !== jsxPragma) {
    return true;
  }

  // "React" or the JSX pragma is referenced as a value if there are any JSX elements in the code.
  let sourceFileHasJsx = false;
  programPath.traverse({
    JSXElement() {
      sourceFileHasJsx = true;
    },
    JSXFragment() {
      sourceFileHasJsx = true;
    },
  });
  return !sourceFileHasJsx;
}