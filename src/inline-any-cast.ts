

// Canonical implementation
// The imports will be removed at compilation
export function any(value) {
  return value;
}

/**
 * Inlines `any(value)`, `string(...value)` and other 
 * cast expressions to `value`.
 */
export default (functionNames = ['any']): Plugin => ({ types: t }) => ({
  visitor: {
    CallExpression(path: NodePath<CallExpression>) {
      const node = path.node
      const args = node.arguments
      const first = args[0]

      if (
        t.isIdentifier(node.callee) &&
        functionNames.includes(node.callee.name) &&
        args.length === 1 &&
        t.isExpression(first)
      ) {
        path.replaceWith(first);
      }
    }
  }
})
