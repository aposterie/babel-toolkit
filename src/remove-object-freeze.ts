

/**
 * Remove `Object.freeze()` call to an object.
 *
 *     Object.freeze({ foo: 'bar' }) --> { foo: 'bar' }
 *
 * @example
 * 
 * babel.transform(code, {
 *   plugins: [
 *     require("babel-plugin-remove-object-freeze")(),
 *   ]
 * })
 */
export default (): Plugin => ({ types: t }) => ({
  visitor: {
    CallExpression(path: NodePath<CallExpression>) {
      const { node } = path
      if (
        t.isMemberExpression(node.callee) &&
        t.isIdentifier(node.callee.object, { name: 'Object' }) &&
        t.isIdentifier(node.callee.property, { name: 'freeze' }) &&
        node.arguments.length === 1
       ) {
        path.replaceWith(path.node.arguments[0])
      }
    },
  },
})
