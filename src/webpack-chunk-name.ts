import { basename } from 'path'

const comment = (value: string) => ({ value, type: 'CommentBlock' })

export default (): Plugin => ({ types: t }) => ({
  visitor: {
    CallExpression({ node }: NodePath<CallExpression>) {
      const arg = node.arguments[0]
      if (
        t.isImport(node.callee)
        && t.isStringLiteral(arg)
        && !arg.leadingComments
      ) {
        const chunkName = JSON.stringify(basename(arg.value))
        arg.leadingComments = [
          comment(` webpackChunkName: ${chunkName} `)
        ] as any
      }
    }
  }
})
