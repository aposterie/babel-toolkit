/**
 * From:
 * ```
 * createObjectReducer([], {
 *   [Actions.TYPE](state, action) {
 *     return { ...state, key: "value" }
 *   },
 * })
 * ```
 *
 * to:
 * ```
 * (state = [], action) => {
 *   switch (action.type) {
 *     case Actions.Type:
 *       return { ...state, key: "value" }
 *     default:
 *       return state;
 *   }
 * };
 * ```
 */
export default (): Plugin => function ({ types: t }) {
  return {
    visitor: {
      CallExpression(path: NodePath<CallExpression>) {
        const { node } = path;
        if (!t.isIdentifier(node.callee, { name: "createObjectReducer" })) return;
        if (node.arguments.length !== 2) return;

        const _state: Identifier = path.scope.generateUidIdentifier("state");
        const _action: Identifier = path.scope.generateUidIdentifier("action");

        const defaultState = node.arguments[0];
        const { properties } = node.arguments[1] as ObjectExpression;

        if (!properties) return;

        const cases: SwitchCase[] = [];

        for (let i = 0; i < properties.length; i++) {
          const prop = properties[i];
          // Bail out for spread elements
          if (t.isSpreadElement(prop)) return;

          const key = prop.key;

          const isObjectProp = t.isObjectProperty(prop);
          const params: any[] = isObjectProp ? (<any>prop).value.params : (<any>prop).params;
          const fn: BlockStatement = isObjectProp ? (<any>prop).value.body : (<any>prop).body;

          // Bail out for default parameters.
          if (params.some(param => !t.isIdentifier(param) && !t.isObjectPattern(param))) return;

          // Rename parameters in scope to avoid conflicts
          const fnPath = path.get(`arguments.1.properties.${i}.${isObjectProp ? "value." : ""}body`) as NodePath<BlockStatement>;

          const handleParam = (param, identifier: Identifier) => {
            if (t.isIdentifier(param)) {
              fnPath.scope.rename(param.name, identifier.name);
            } else {
              fn.body.unshift(
                t.variableDeclaration('const', [
                  t.variableDeclarator(param as ObjectPattern, identifier)
                ])
              );
            }
          }

          if (params[0]) {
            handleParam(params[0], _state);
          }
          if (params[1]) {
            handleParam(params[1], _action);
          }

          // Avoid unnecessary {} codes.
          let caseBody: Statement = fn;
          if (fn.body.length === 1) {
            caseBody = fn.body[0] as any;
          }

          cases.push(t.switchCase(key, [caseBody]));
        }

        // Deal with the case of no match
        cases.push(t.switchCase(null, [t.returnStatement(_state)]));

        path.replaceWith(
          t.arrowFunctionExpression(
            [
              // state = defaultState
              t.assignmentPattern(
                _state,
                t.isArrowFunctionExpression(defaultState)
                  ? t.callExpression(defaultState, [])
                  : defaultState as Expression
              ),
              _action
            ],
            t.blockStatement([
              t.switchStatement(t.memberExpression(_action, t.identifier("type")), cases)
            ])
          )
        );
      }
    }
  };
}