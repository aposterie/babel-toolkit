
/**
 * Expands a [].includes() check to `===`, except for explicit `NaN` value.
 */
export default ({ handleNaN = true }): Plugin => ({ types: t }) => ({
  visitor: {
    CallExpression(path: NodePath<CallExpression>) {
      const { node } = path;
      const args = node.arguments as Expression[];
      if (!t.isMemberExpression(node.callee)) return;

      const { object, property } = node.callee;

      if (
        t.isArrayExpression(object) &&
        t.isIdentifier(property, { name: 'includes' }) &&
        object.elements.length > 0 &&
        args.length === 1
      ) {
        const value = args[0];
        const comparison = object.elements.map((el: Expression) => {
          if (handleNaN && t.isIdentifier(el) && el.name === 'NaN') {
            return t.binaryExpression('!==', value, value);
          } else {
            return t.binaryExpression('===', value, el);
          }
        }).reduce<LogicalExpression>(
          (accum, current) => t.logicalExpression('||', accum, current)
        );

        path.replaceWith(comparison);
      }
    }
  }
});
