import template from '@babel/template';

const buildForOfArray = template(`
  for (var KEY = 0; KEY < ARR.length; KEY++) BODY;
`);

function ForOfStatementArray(path: NodePath<ForOfStatement>, t: types) {
  const { node, scope } = path
  const nodes: Node[] = []
  let right = node.right

  if (!(t.isIdentifier(right) && scope.hasBinding(right.name))) {
    const uid = scope.generateUidIdentifier('arr')
    const decl = t.variableDeclarator(uid, right)
    nodes.push(t.variableDeclaration('var', [decl]));
    right = uid;
  }

  const iterationKey = scope.generateUidIdentifier('i')

  let iter = buildForOfArray({
    BODY: node.body,
    KEY:  iterationKey,
    ARR:  right,
  }) as ForStatement<BlockStatement>;

  t.inherits(iter, node)
  t.ensureBlock(iter);

  const iterationValue = t.memberExpression(right, iterationKey, true);

  const left = node.left
  if (t.isVariableDeclaration(left)) {
    left.declarations[0].init = iterationValue;
    iter.body.body.unshift(left);
  } else {
    const assignment = t.assignmentExpression('=', left, iterationValue);
    iter.body.body.unshift(t.expressionStatement(assignment));
  }

  if (path.parentPath.isLabeledStatement()) {
    nodes.push(t.labeledStatement(path.parentPath.node.label, iter));
  } else {
    nodes.push(iter);
  }
  return nodes;
}

export default (): Plugin => ({ types: t }) => ({
  visitor: {
    ForOfStatement(path: NodePath<ForOfStatement>) {
      if (path.parentPath.isLabeledStatement()) {
        path.parentPath.replaceWithMultiple(ForOfStatementArray(path, t))
      } else {
        path.replaceWithMultiple(ForOfStatementArray(path, t))
      }
    }
  }
});