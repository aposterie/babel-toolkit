export function orElse<A, B>(value: A | null | undefined, fallback: B) {
  return value == null ? fallback : value
}

export function ensureArray<T>(value: T[] | null | undefined | T) {
  return value == null ? [] : Array.isArray(value) ? value : [value]
}

export function none<T>(list: T[], predicate: (value: T, index: number, list: T[]) => boolean) {
  return list.every((item, i, list) => !predicate(item, i, list))
}

/**
 * Check a member expression
 */
export function isMemberExpressionLiteral(t: types, exp: Expression, memberExpression: string): exp is MemberExpression {
  const [ left, right ] = memberExpression.split('.');
  return (
    t.isMemberExpression(exp) &&
    t.isIdentifier(exp.object, { name: left }) &&
    t.isIdentifier(exp.property, { name: right })
  );
}

export function tryEval<T>(fn: () => T) {
  try { return fn() } catch {}
}

export function generateUidIdentifier(scope: Scope, identifier: Identifier) {
  if (scope.hasBinding(identifier.name)) {
    return scope.generateUidIdentifier(identifier.name);
  } else {
    return identifier
  }
}

export function renameIfCollides(scope: Scope, oldName: string, newName: string) {
  if (oldName === newName) {
    // noop
  } if (scope.hasBinding(newName)) {
    scope.rename(oldName, scope.generateUidIdentifier(newName).name);
  } else {
    scope.rename(oldName, newName);
  }
}
