export default (): Plugin => ({ types: t }) => ({
  visitor: {
    TSNonNullExpression(path) {
      const { node, parent, parentPath } = path;
      if (t.isMemberExpression(parent)) {
        parentPath.replaceWith(
          t.optionalMemberExpression(
            node.expression,
            parent.property,
            parent.computed,
            true
          )
        )
      }
    }
  }
})
