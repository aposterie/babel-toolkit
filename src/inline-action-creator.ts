
/**
 * Inlines `action(ACTION_TYPE, { props })` to
 * `{ type: ACTION_TYPE, ...props }`.
 */
export default (functionName = 'action'): Plugin => ({ types: t }) => ({
  visitor: {
    CallExpression(path: NodePath<CallExpression>) {
      const node = path.node
      const args = node.arguments
      const [ first, second ] = args

      t.optionalMemberExpression

      if (
        t.isIdentifier(node.callee, { name: functionName }) &&
        args.length >= 1 &&
        (t.isIdentifier(first) || t.isLiteral(first))
      ) {
        const typeProperty = t.objectProperty(t.identifier('type'), first as Identifier | Literal)

        // action(ACTION_TYPE)
        if (args.length === 1) {
          path.replaceWith(t.objectExpression([typeProperty]))
        }

        // action(ACTION_TYPE, object)
        if (args.length === 2 && t.isIdentifier(second)) {
          const literal = t.callExpression(
            t.memberExpression(t.identifier('Object'), t.identifier('assign')),
            [t.objectExpression([typeProperty]), second]
          )
          path.replaceWith(literal);
        }

        // action(ACTION_TYPE, { additionProperties })
        if (args.length === 2 && t.isObjectExpression(second)) {
          const literal = second;
          literal.properties.unshift(typeProperty);
          path.replaceWith(literal);
        }
      }
    }
  }
})