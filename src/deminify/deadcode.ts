export const removeDeadCode: Plugin = ({ types: t }) => ({
  visitor: {
    ExpressionStatement(path: NodePath<ExpressionStatement>) {
      const { node } = path;
      const { expression } = node;
      if (
        t.isMemberExpression(expression) &&
        t.isIdentifier(expression.object) &&
        t.isIdentifier(expression.property)
      ) {
        // useless member expressions
        path.remove();
      }
    },
  }
});
