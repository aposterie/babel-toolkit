import { renameIfCollides } from '../@util';

export const rename: Plugin = ({ types: t }) => ({
  visitor: {
    VariableDeclaration(path: NodePath<VariableDeclaration>) {
      const { node, scope } = path;
      for (const dec of node.declarations) {
        // Avoid visiting the declaration again after modifying it
        if (t.isVariableDeclarator(dec) && t.isIdentifier(dec.id) && dec.id.name.length <= 2) {
          // var s = this;
          // var _this = this;
          if (t.isThisExpression(dec.init!)) {
            renameIfCollides(scope, dec.id.name, '_this');
          } else if (
            t.isMemberExpression(dec.init!) &&
            t.isIdentifier(dec.init.property) &&
            dec.init.property.name.length > 2
          ) {
            renameIfCollides(scope, dec.id.name, dec.init.property.name);
          }
        }
      }
    },

    UnaryExpression(path: NodePath<UnaryExpression>) {
      const { node } = path;
      const { operator, argument: arg } = node;
      // void 0
      if (operator === 'void' && t.isNumericLiteral(arg)) {
        path.replaceWith(t.identifier('undefined'));
      } else if (operator === '!' && t.isNumericLiteral(arg)) {
        if (arg.value === 1) {
          // !1
          path.replaceWith(t.booleanLiteral(false));
        } else if (arg.value === 0) {
          // !0
          path.replaceWith(t.booleanLiteral(true));
        }
      } else if (operator === '!' && t.isCallExpression(arg) && t.isFunctionExpression(arg.callee)) {
        // !(function (e) {})(e)
        path.replaceWith(t.callExpression(arg.callee, arg.arguments));
      }
    },

    CallExpression(path: NodePath<CallExpression>) {
      const { node } = path;
      const { callee, arguments: args } = node;
      const arg = args[0];
      if (t.isFunctionExpression(callee)) {
        /**
         * (a => {})(RealName) ->
         * (RealName => {})(RealName)
         */
        const { params } = callee;
        const functionPath = path.get('callee');
        for (let i = 0; i < args.length; i++) {
          const arg = args[i];
          const paramId = params[i];
          if (t.isIdentifier(arg) && t.isIdentifier(paramId) && paramId.name.length === 1) {
            renameIfCollides(functionPath.scope, paramId.name, arg.name);
          }
        }
      } else if (
        // createCommonjsModule(( **module**, **exports** ) => ...)
        t.isIdentifier(callee, { name: 'createCommonjsModule' }) &&
        args.length === 1 &&
        (t.isArrowFunctionExpression(arg) || t.isFunctionExpression(arg)) &&
        arg.params.length >= 1 &&
        arg.params.every(param => param.name.length === 1)
      ) {
        const params = arg.params as Identifier[];
        const childPath = path.get('arguments.0') as NodePath<ArrowFunctionExpression | FunctionExpression>;

        if (t.isReferenced(params[0], childPath.scope)) {
          renameIfCollides(childPath.scope, params[0].name, 'module');
        }
        if (params[1]) {
          renameIfCollides(childPath.scope, params[1].name, 'exports');
        }
      }
    },
  }
});

