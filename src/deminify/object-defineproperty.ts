import { isMemberExpressionLiteral, tryEval } from '../@util';

/**
 * Merge multiple `Object.defineProperties`.
 */
function mergeObjectDefineProperties(t: types, path: NodePath<CallExpression>) {
  const { node, parentPath } = path;
  const { callee, arguments: args } = node;
  const nextDoor = parentPath.getSibling(Number(parentPath.key) + 1);
  const nextExp = tryEval(() => (<any>nextDoor).node.expression);
  const nextObj = tryEval(() => nextExp.arguments[1].arguments[0]);

  const descriptorSource = args[1];
  if (
    isMemberExpressionLiteral(t, callee, 'Object.defineProperties') &&
    t.isCallExpression(descriptorSource) &&
    isMemberExpressionLiteral(t, descriptorSource.callee, 'Object.getOwnPropertyDescriptors') &&
    nextObj &&
    isMemberExpressionLiteral(t, nextExp.callee, 'Object.defineProperties') &&
    t.isObjectExpression(descriptorSource.arguments[0]) &&
    t.isObjectExpression(nextObj)
  ) {
    const obj = <ObjectExpression>descriptorSource.arguments[0];
    obj.properties.push(...nextExp.arguments[1].arguments[0].properties);
    nextDoor.remove();

    return true;
  }
}

/**
 * Object.defineProperty(Object, 'prop', { get() {}, set() {} }) ->
 * Object.defineProperties(Object, Object.getOwnPropertyDescriptors({ get prop() {}, set prop() {} }))
 */
function literalObjectDefineProperty(t: types, path: NodePath<CallExpression>) {
  const { node } = path;
  const { callee, arguments: args } = node;

  const literal = args[1];
  const propsObj = args[2];

  if (
    // Object.defineProperty
    isMemberExpressionLiteral(t, callee, 'Object.defineProperty') &&
    t.isStringLiteral(literal) &&
    literal.value !== '__esModule' &&
    t.isObjectExpression(propsObj) &&
    propsObj.properties.some(v => t.isObjectProperty(v) && t.isIdentifier(v.key) && ['get', 'set'].includes(v.key.name))
  ) {
    (callee.property as Identifier).name = 'defineProperties';
    const props = <ObjectMethod[]>propsObj.properties.filter(v => t.isObjectMethod(v));
    for (const prop of props) {
      const key = prop.key as Identifier;
      if (key.name === 'get') {
        prop.kind = 'get';
        key.name = literal.value;
      } else if (key.name === 'set') {
        prop.kind = 'set';
        key.name = literal.value;
      }
    }

    propsObj.properties = props;
    args[2] = t.callExpression(
      t.memberExpression(
        t.identifier('Object'),
        t.identifier('getOwnPropertyDescriptors')
      ),
      [propsObj]
    );
    args.splice(1, 1);
    return true;
  }
}

export const defineProperty: Plugin = ({ types: t }) => ({
  visitor: {
    CallExpression(path: NodePath<CallExpression>) {
      (mergeObjectDefineProperties(t, path) ||
      literalObjectDefineProperty(t, path));
    },
  }
});
