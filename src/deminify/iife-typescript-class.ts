/**
 * Remove IIFE Wrapper for
 * ```ts
 * var Xe = (function () {
 *    function Class() {}
 *    __extends(Class, superClass);
 *    Class.prototype.method = ...;
 *    Object.defineProperties(Class.prototype, ...);
 *    return Class;
 * })();
 * ```
 */
export const iifeTypeScriptClass: Plugin = ({ types: t }) => ({
  visitor: {
    VariableDeclaration(path: NodePath<VariableDeclaration>) {
      const { node } = path;
      const [ dec ] = node.declarations;
      const { id, init } = dec;
      if (
        t.isIdentifier(id) &&
        node.declarations.length === 1 &&
        t.isCallExpression(init!) &&
        t.isFunctionExpression(init.callee) &&
        // Either no argument or arguments and params have exactly the same names
        // i.e. ((arg1, arg2) => {})(arg1, arg2)
        (!init.arguments.length || init.arguments.every((arg: Identifier, i) => t.isIdentifier((<FunctionExpression>init.callee).params[i], { name: arg.name })))
      ) {
        const body = init.callee.body.body.slice();
        const funcDec = body.shift();
        const returnState = body.pop();

        if (!t.isFunctionDeclaration(funcDec!)) return;
        if (!t.isReturnStatement(returnState!)) return;

        if (body.length &&
          t.isIdentifier(funcDec.id!, { name: id.name }) &&
          body.every(node => t.isExpressionStatement(node))
        ) {
          path.replaceWithMultiple([funcDec, ...body]);
        }
      }
    }
  }
});
