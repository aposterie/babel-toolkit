export const defaultParam: Plugin = ({ types: t }) => ({
  visitor: {
    FunctionDeclaration(path: NodePath<FunctionDeclaration>) {
      defaultParamWalk(t, path);
    },
    FunctionExpression(path: NodePath<FunctionExpression>) {
      defaultParamWalk(t, path);
    },
  }
})

function defaultParamWalk(t: types, path: NodePath<FunctionDeclaration | FunctionExpression>) {
  const { node } = path;
  const { params, body: { body } } = node;
  const statements: Statement[] = [];

  for (const state of body) {
    let matchParam: Identifier;
    // if (undefined == param) {
    //   param = defaultValue;
    // }
    if (
      // if
      t.isIfStatement(state) &&
      // (
      t.isBinaryExpression(state.test) &&
      // undefined
      t.isIdentifier(state.test.left, { name: 'undefined' }) &&
      // ===
      state.test.operator === '===' &&
      // identifier
      t.isIdentifier(state.test.right) &&
      // and is a parameter of this function
      (matchParam = <Identifier>params.find(param => t.isIdentifier(param, { name: (<any>state.test).right.name }))) &&
      // and has no `else` block, has one assignment in the `if` block
      !state.alternate && t.isBlockStatement(state.consequent) && t.isExpressionStatement(state.consequent.body[0])
    ) {
      const index = params.indexOf(matchParam);
      params[index] = t.assignmentPattern(
        matchParam,
        (<any>(<ExpressionStatement>(state.consequent.body[0])).expression).right
      );
    } else {
      statements.push(state);
    }
  }

  node.body.body = statements;

  // Always fall back to next plugin.
  return false;
}
