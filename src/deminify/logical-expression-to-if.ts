export const logicalExpressionToIf: Plugin = ({ types: t }) => ({
  visitor: {
    ReturnStatement(path: NodePath<ReturnStatement>) {
      const { node } = path;
      const { argument } = node;

      if (t.isConditionalExpression(argument!)) {
        // return cond ? exp1 : exp2;
        path.replaceWith(t.ifStatement(
          argument.test,
          t.blockStatement([t.returnStatement(argument.consequent)]),
          t.blockStatement([t.returnStatement(argument.alternate)])
        ));
      }
    },

    ExpressionStatement(path: NodePath<ExpressionStatement>) {
      const { node } = path;
      const { expression } = node;
      if (t.isLogicalExpression(expression)) {
        if (expression.operator === '&&') {
          // (cond == value) && doStuff()
          path.replaceWith(t.ifStatement(
            expression.left,
            t.blockStatement([t.expressionStatement(expression.right)])
          ));
        } else if (expression.operator === '||') {
          // (cond == value) || doStuff()
          path.replaceWith(t.ifStatement(
            t.unaryExpression('!', expression.left),
            t.blockStatement([t.expressionStatement(expression.right)])
          ));
        }
      }
    },
  }
});
