export const desequence: Plugin = ({ types: t }) => ({
  visitor: {
    ReturnStatement(path: NodePath<ReturnStatement>) {
      const { node } = path;
      const { argument } = node;

      if (t.isSequenceExpression(argument!)) {
        // return 1, 0, c;
        const expressions = argument.expressions.slice();
        const last = expressions.pop();

        path.replaceWithMultiple([...expressions.map(e => t.expressionStatement(e)), t.returnStatement(last)]);
      }
    },

    VariableDeclaration(path: NodePath<VariableDeclaration>) {
      const { node } = path;
      const [ dec ] = node.declarations;
      if (node.declarations.length === 1 && t.isSequenceExpression(dec.init!)) {
        const exps = dec.init.expressions.slice();
        const last = exps.pop();
        dec.init = last!;
        path.insertBefore(exps.map(e => t.expressionStatement(e)));
      }
    },

    ExpressionStatement(path: NodePath<ExpressionStatement>) {
      const { node } = path;
      const { expression } = node;
      if (t.isSequenceExpression(expression)) {
        // doStuff(), doStuff2(), ...
        path.replaceWithMultiple(expression.expressions.map(e => t.expressionStatement(e)));
      } else if (t.isConditionalExpression(expression)) {
        // cond ? send() : reject()
        path.replaceWith(t.ifStatement(
          expression.test,
          t.blockStatement([t.expressionStatement(expression.consequent)]),
          t.blockStatement([t.expressionStatement(expression.alternate)])
        ));
      }
    },

    IfStatement(path: NodePath<IfStatement>) {
      const { node } = path;
      if (t.isSequenceExpression(node.test)) {
        // if (doStuff(), assign = 2, realCondition) {}
        const expressions = node.test.expressions.slice();
        const last = expressions.pop();
        node.test = last!;
        path.insertBefore(expressions.map(e => t.expressionStatement(e)));
      } else if (
        t.isBlockStatement(node.alternate!) &&
        node.alternate.body.length === 1 &&
        t.isIfStatement(node.alternate.body[0])
      ) {
        // if (...) else { if (...) }
        node.alternate = node.alternate.body[0];
      }
    }
  }
});
