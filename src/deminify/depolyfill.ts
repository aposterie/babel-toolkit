import { isMemberExpressionLiteral } from '../@util';
/**
 * Remove polyfill codes
 */
export const depolyfill: Plugin = ({ types: t }) => ({
  visitor: {
    /**
     * Remove right-hand side of binary expressions as `Object.keys ||`, etc.
     */
    LogicalExpression(path: NodePath<LogicalExpression>) {
      const { node } = path;
      const { left } = node;
      if (
        isMemberExpressionLiteral(t, left, 'Object.keys') ||
        isMemberExpressionLiteral(t, left, 'Object.assign')
      ) {
        path.replaceWith(left);
      }
    },
    BinaryExpression(path: NodePath<BinaryExpression>) {
      const { node } = path;
      const { left, right } = node;
      /**
       * Inline `typeof Symbol`, etc. checks
       */
      if (
        t.isUnaryExpression(left) &&
        left.operator === 'typeof' &&
        t.isStringLiteral(right, { value: 'function' })
      ) {
        if (t.isIdentifier(left.argument, { name: 'Symbol' })) {
          path.replaceInline(t.booleanLiteral(true));
        }
      }
    },
  }
})
