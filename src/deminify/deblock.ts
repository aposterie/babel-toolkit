/**
 * (param) => { return 0; } ->
 * (param) => 0;
 */
export const deblock: Plugin = ({ types: t }) => ({
  visitor: {
    IfStatement(path: NodePath<IfStatement>) {
      // if (true) ...
      const { node } = path;
      if (t.isBooleanLiteral(node.test, { value: true })) {
        if (t.isBlockStatement(node.consequent)) {
          path.replaceWith(node.consequent);
        } else {
          path.replaceWith(node.consequent);
        }
      } else if (t.isBooleanLiteral(node.test, { value: false })) {
        path.remove();
      }
    },
    ArrowFunctionExpression(path: NodePath<ArrowFunctionExpression>) {
      const { node } = path;
      const { params, body } = node;
      if (
        t.isBlockStatement(body) &&
        t.isReturnStatement(body.body[0])
      ) {
        path.replaceWith(
          t.arrowFunctionExpression(
            params,
            (body.body[0] as ReturnStatement).argument!,
            node.async
          )
        );
      }
    },
  }
});
