
import * as _ from 'lodash'

const packageName = require('./package.json').name;

// Canonical implementation
function range(stop: number): IterableIterator<number>;
function range(start: number, stop: number): IterableIterator<number>;
function range(start: number, stop: number, step: number): IterableIterator<number>;

function *range(first: number, stop?: number, step?: number) {
  if (arguments.length === 1) {
    yield *range(0, first, 1)
  } else {
    step = step == null ? 1 : step;
    for (let i = first; i < stop!; i += step) {
      yield i;
    }
  }
}

// Import this function to the module that needs `range`
// The import will be removed at compilation
export { range }

/**
 * Inline Python’s `range()` function when used in a for-of loop.
 * @example
 * range(stop)
 * range(start, stop)
 * range(start, stop, step)
 */
export default (): Plugin => ({ types: t }) => ({
  visitor: {
    ImportDeclaration(path: NodePath<ImportDeclaration>) {
      const { node } = path
      const specifier = node.specifiers[0]
      if (
        node.specifiers.length === 1 &&
        t.isImportSpecifier(specifier) &&
        t.isIdentifier(specifier.local, { name: 'range' }) &&
        t.isStringLiteral(node.source, { value: packageName })
      ) {
        path.remove()
      }
    },

    ForOfStatement(path: NodePath<ForOfStatement>) {
      const { node } = path;
      const right = node.right as CallExpression;
      const left = node.left as VariableDeclaration | Identifier;
      const args = right.arguments as Expression[];

      if (
        t.isCallExpression(right) &&
        t.isIdentifier(right.callee, { name: 'range' }) &&
        _.inRange(args.length, 1, 3)
      ) {
        const begin = args.length === 1 ? args[0] : t.numericLiteral(0);
        const end = args.length === 1 ? args[0] : args[1];
        const hasStep = args.length === 3;

        const identifer = t.isVariableDeclaration(left)
          ? left.declarations[0].id as Identifier
          : left;

        const init = t.isVariableDeclaration(left)
          ? t.variableDeclaration(left.kind, [t.variableDeclarator(identifer, begin)])
          : t.assignmentExpression('=', identifer, begin);

        const test = t.binaryExpression('<', identifer, end);
        const update = hasStep
          ? t.assignmentExpression('+=', identifer, args[2])
          : t.updateExpression('++', identifer);

        path.replaceWith(t.forStatement(init, test, update, node.body));
      }
    }
  }
});