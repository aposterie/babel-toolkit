# babel-toolkit
A collection of useful Babel plugins.

* **typescript-optional-chaining**: Rewrite `!` to the safe navigation operator.
* **emotion-strip**: Extract [emotion](https://emotion.sh) CSS strings to file and replace them with static class names.
* **expand-includes**: Expands `[].includes` into a list of comparison expressions.
* **inline-action-creator**: Expands `action(ACTION_TYPE, { payload })` calls into object literal.
* **inline-any-cast**: Adds `any()` as a TypeScript casting function that disappears after compilation.
* **range**: Expands Python-like `range()` function into a regular `for` loop
* **strip-symbol-description**: Removes description argument to `Symbol` constructor
* **typescript-enum**: Replaces TypeScript enum IIFE with a straightforward object literal.

## Build

You need [jq](https://stedolan.github.io/jq/) to build.

```sh
yarn
yarn build
```

## License
MIT.
