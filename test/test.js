#!/usr/local/bin/node
const babel = require("@babel/core");
const fs = require("fs-extra");

require("ts-node/register/transpile-only")

async function printAST(source) {
  const ast = await babel.parseAsync(source, {
    plugins: [ require("@babel/plugin-syntax-typescript").default ]
  });
  console.log(ast.program.body);
}

async function main() {
  const source = `
    class A {}
    export type a = number;

    export type b = {
      
    };

    function test(
      num: number,
      str: string,
      bool: boolean,
      seven: 7,
      nully: null,
      undef: undefined,
      bigint: bigint,
      test: "test",
      falsy: false,
      numOrBool: number | boolean,
      numArray: number[],
      unknown: unknown,
      invalid: AABB,
      valid: A,
      func: Function,
      literal: {
        num: number
        right: number
      },
      refA: a,
    ) {
      return num + str;
    }
  `


  const result = await babel.transformAsync(source, {
    plugins: [
      require("../src/type-check/index").default(),
      "@babel/transform-typescript"
    ]
  })

  const { code } = result;
  console.log(code);
}

main().catch(console.error);