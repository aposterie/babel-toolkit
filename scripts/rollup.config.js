import cjs from 'rollup-plugin-commonjs';
import babel from 'rollup-plugin-babel';

const extensions = ['.ts', '.js']
export default {
  output: {
    format: 'cjs',
    banner: '/* eslint-disable */',
  },
  context: 'this',
  external: 'lodash path create-emotion'.split(' '),
  plugins: [
    babel({
      extensions,
      comments: false,
      minified: false,
      presets: [],
      plugins: [
        '@babel/plugin-transform-shorthand-properties',
        '@babel/plugin-transform-arrow-functions',
        '@babel/plugin-transform-typescript',
        // '@babel/plugin-transform-block-scoping',
      ]
    }),
    cjs({
      extensions,
      ignoreGlobal: true,
    }),
  ],
}