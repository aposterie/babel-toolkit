#!/bin/bash
main() {
  for file in $(ls src/*.ts | grep -v .test.ts | grep -v @util.ts); do
    name=$(basename "$file" .ts)

    echo Handling: $name
    rollup \
      --config "scripts/rollup.config.js" \
      --input "./$file" \
      --file "packages/$name.js"
  done
}

require() {
  if ! type "$1" > /dev/null; then
    echo "Error: Cannot find module '$1'"
    exit 1
  fi
}

require "jq"
require "rollup"
main
